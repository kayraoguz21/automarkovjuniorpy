===================== Arguments =====================
name of the output file:        output.txt
novelty factor:                 0.0
coherency factor:               0.99
population size:                100
mutation rate:                  0.1
window size (for rule making):  3
macro evaluation window size:  4
middle evaluation window size:  3
micro evaluation window size:   2
maximum grammar length:         10
diversity factor:               0
=====================================================
