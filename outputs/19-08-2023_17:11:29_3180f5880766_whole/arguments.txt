===================== Arguments =====================
name of the output file:        output.txt
novelty factor:                 0.0
coherency factor:               1
population size:                100
mutation rate:                  0.25
window size (for rule making):  4
macro evaluation window size:  6
middle evaluation window size:  4
micro evaluation window size:   2
maximum grammar length:         10
diversity factor:               0
=====================================================
