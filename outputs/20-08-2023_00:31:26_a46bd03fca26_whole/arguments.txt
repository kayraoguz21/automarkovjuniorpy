===================== Arguments =====================
name of the output file:        output.txt
novelty factor:                 0.0
coherency factor:               0.9
population size:                100
mutation rate:                  0.1
window size (for rule making):  6
macro evaluation window size:  8
middle evaluation window size:  6
micro evaluation window size:   3
maximum grammar length:         10
diversity factor:               0
=====================================================
