## Automated PCG with MarkovJuniorPy PCG Framework 

# This framework is capable of genrating SUper Mario level with automated PCG with Markov Algorithm. The Markov Algprithm PCG framework, inspired by the MarkovJunior https://github.com/mxgmn/MarkovJunior.  
--------------------------------------------------------------------------------------------------------

Dependencies 
------------

Recommended Python Version
    Python 3.10.10

Used Libraries

    numba==0.57.0
    numpy==1.24.3
    opencv_python==4.7.0.72
    Pillow==9.5.0
    Pillow==10.0.0
    psutil==5.9.5
    pygame==2.5.0

Mehmet Kayra Oguz 2023